import { BasketComponent } from './basket/basket.component';
import { HomeComponent } from './home/home.component';
import { Routes } from "@angular/router/router";
import { RouterModule } from '@angular/router';
import { ModuleWithProviders } from "@angular/core/core";

const path: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'basket', component: BasketComponent }
];

const appRoutes: ModuleWithProviders = RouterModule.forRoot(path)

export default appRoutes;