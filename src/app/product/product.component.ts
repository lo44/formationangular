import { ProductService } from './../service/product.service';
import { Product } from './../model/product';
import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(private productService: ProductService) { }

  @Input('data') product: Product;
  @Input('showLink') showLink: boolean;
  @Output() addToBasketEvent: EventEmitter<Product> = new EventEmitter();

  ngOnInit() {
  }

  /**
   * 
   * @param product 
   * @returns 
   */
  addToBasket(product: Product) {
    this.addToBasketEvent.emit(product);
  }

  isTheLastProduct(product: Product) {
    return this.productService.isTheLastProduct(product)
  }

}