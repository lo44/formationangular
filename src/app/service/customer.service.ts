import { Customer } from './../model/customer';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Product } from './../model/product';
import { Injectable } from '@angular/core';

@Injectable()
export class CustomerService {

  private basket: Product[] = [];
  constructor(private http: Http) { }

  addProduct(product: Product): Observable<Response> {
    return this.http.post('http://localhost:8080/rest/basket', product)
      .map(res => res.json())
      .do(res => this.basket.push(product))
  }

  getTotal() {
    let total = 0;
    this.basket.forEach(item => { total += item.price })
    return total;
  }

  getBasket(): Observable<Product[]> {
    return this.http.get('http://localhost:8080/rest/basket')
      .map(res => res.json())
      .do(basket => this.basket = basket);
  }

  checkout(customer: Customer): Observable<Response> {
    return this.http.post('http://localhost:8080/rest/basket/confirm', customer)
  }

}
