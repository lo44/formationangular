import { Product } from './../model/product';
import { Injectable } from '@angular/core';
import { UpperCasePipe } from '@angular/common'
import { Http, Response } from '@angular/http'
import { Observable } from "rxjs/Observable";

@Injectable()
export class ProductService {
  
  private upperCasePipe: UpperCasePipe = new UpperCasePipe();

  constructor(private http: Http) {

  }

  getProducts(): Observable<Product[]> {
    return this.http.get('http://localhost:8080/rest/products')
      .map((response: Response) => response.json())
      .do((products :Product[]) => products.forEach(product => product.title = this.upperCasePipe.transform(product.title) ));
  }

  isTheLastProduct(product: Product) {
    return product.stock === 1;
  }

  isAvailable(product: Product) {
    return product.stock > 0;
  }

  decreaseStock(product: Product) {
    product.stock -= 1;
  }

}
