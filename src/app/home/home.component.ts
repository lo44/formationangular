import { CustomerService } from './../service/customer.service';
import { ProductService } from './../service/product.service';
import { Product } from './../model/product';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  products: Product[] = [];

  constructor(private productService: ProductService, private customerService: CustomerService, @Inject('title') public title) { }

  ngOnInit() {
    this.productService.getProducts()
      .subscribe(products => this.products = products);
    this.customerService.getBasket().subscribe();
  }

  addToBasket(product: Product) {
    this.customerService.addProduct(product)
      .subscribe(succes => console.log("Product added into basket"), error => "Error");
    this.productService.decreaseStock(product);
  }

  getTotalBasket() {
    return this.customerService.getTotal();
  }

  isAvailable(product: Product) {
    return this.productService.isAvailable(product);
  }

}
