import { Product } from './../model/product';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterProduct'
})
export class FilterProductPipe implements PipeTransform {

  transform(list: Product[], atr: string = 'title'): any {
    return  list.sort((a,b) =>  a[atr] > b[atr] ? 1 : -1);
  }

}
