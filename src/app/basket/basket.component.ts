import { Router } from '@angular/router';
import { Customer } from './../model/customer';
import { Product } from './../model/product';
import { CustomerService } from './../service/customer.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  public basket: Product[] = [];
  public customer: Customer = new Customer('', '', '');


  constructor(private customerService: CustomerService, private router: Router) { }

  ngOnInit() {
    console.log('');
    this.customerService.getBasket().subscribe(basket => this.basket = basket);
  }

  saveForm() {
    this.customerService.checkout(this.customer).subscribe(res => console.log('Commande prise'));
    this.router.navigate(['home']);
  }

}
