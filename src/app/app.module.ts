import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import localeFrCa from '@angular/common/locales/fr';
import localeFrCaExtra from '@angular/common/locales/extra/fr';
// Services
import { ProductService } from './service/product.service';
import { CustomerService } from './service/customer.service';
// Components
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { ProductComponent } from './product/product.component';
import { FooterComponent } from './footer/footer.component';
import { FilterProductPipe } from './pipe/filter-product.pipe';
import { HomeComponent } from './home/home.component';
import { BasketComponent } from './basket/basket.component';
// OTHERS
import './rxjs-operators';
import AppRoutes from './app.routing';

registerLocaleData(localeFrCa, localeFrCaExtra);


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ProductComponent,
    FooterComponent,
    FilterProductPipe,
    HomeComponent,
    BasketComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutes,
    FormsModule    
  ],
  providers: [
    CustomerService,
    ProductService,
    { provide: 'title', useValue: 'Bienvenue sur Zenika Ecommerce' },
    { provide: LOCALE_ID, useValue: 'fr' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
