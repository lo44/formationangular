export class Product {
    
    constructor(public title: string, public description: string, public price: number, public photo: string, public stock: number) { }
}